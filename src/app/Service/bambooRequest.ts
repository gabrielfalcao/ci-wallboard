'use strict';
import {Injectable} from '@angular/core';
import {Settings} from './settings';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import 'rxjs/Rx';
import {AbstractCiRequest} from './abstractCiRequest';
// Models.
import {BambooBuild} from '../Component/Builds/bamboo.build.model'
import {Commit} from '../Component/Builds/commit.model'
import {RepoSettings} from '../Component/Setting/reposettings.model'
import {Project} from '../Component/Project/project.model'

@Injectable()
export class BambooRequest extends AbstractCiRequest {

    constructor(http: Http, settings: Settings) {
        super(http, settings);
    }

    /**
     * Load project data from bamboo.
     *
     * @param {RepoSettings} settings
     * @returns {Observable<R>}
     */
    loadProjectData(settings: RepoSettings) {

        let uri = settings.getToken();
        //Remove placeholder
        uri = uri.replace('{{', '');
        uri = uri.replace('}}', '');

        let headers = new Headers({
            'Accept': this.settings.getAcceptHeader(),
        });
        let options = new RequestOptions({headers: headers});

        return this.http.get(uri, options).map(res => {
                let repos = res.json().results;
                let repositories = [];
                for (let repo of repos.result) {
                    let projectModel = new Project(
                        repo.plan.key,
                        repo.key,
                        true,
                        repo.buildResultKey,
                        '1',
                        '1',
                        repo.buildNr,
                        repo.buildNr,
                        repo.buildState
                    );
                    repositories.push(projectModel);
                }
                return repositories;
            }
        );
    }

    /**
     * Retrieve a single travis build.
     *
     * @param {RepoSettings} settings
     * @param {string} slug
     * @param id
     * @param {Integer} buildId
     * @returns {Observable<R>}
     */
    getBuild(settings: RepoSettings, slug: string, id: number, buildId) {
        console.info('implement me');
    }

    /**
     *
     * @param {RepoSettings} settings
     * @param {string} slug
     *
     * @param id
     * @param all
     * @returns {Observable<R>}
     */
    getBuilds(settings: RepoSettings, slug: string, id: number, all: boolean) {
        let uri = settings.getToken();
        //Remove placeholder
        uri = uri.replace(/{{.*?}}/, slug);

        let headers = new Headers({
            'Accept': this.settings.getAcceptHeader(),
        });
        let options = new RequestOptions({headers: headers});

        return this.http.get(uri, options).map(res => {
                let repo = res.json();
                let builds = [];
                let build = this.createBuildModel(repo, {}, slug);
                builds.push(build);
                return builds;
            }
        );
    }

    /**
     * Create a build mode from the retreived data.
     *
     * @param {Object} data
     * @param {Object} commit
     * @param {string} slug
     *
     * @returns {Build} Build object.
     */
    createBuildModel(data: any, commit: any, slug: string) {
        let pattern = /changes by <.*?>(.*?)</i;
        let name = 'unknown';
        let full = 'unknown';

        if (typeof data.buildReason !== 'undefined') {
            let nameParts = data.buildReason.match(pattern);
            name = nameParts[1];
            full = nameParts[2];
        }
        let commitModel = new Commit(
            data.id,
            '',
            name,
            name,
            full,
            '1'
        );
        return new BambooBuild(
            data.id,
            data.number,
            slug,
            data.state,
            data.buildStartedTime,
            data.buildCompletedTime,
            false,
            data.number,
            data.event_type,
            commitModel
        );

    }

    handleError(error: Response): void {

    }
}
