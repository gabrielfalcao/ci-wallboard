'use strict';
import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import {Settings} from './settings';
import 'rxjs/Rx';
import {AbstractCiRequest} from './abstractCiRequest';
// Models.
import {Build} from '../Component/Builds/build.model'
import {Commit} from '../Component/Builds/commit.model'
import {RepoSettings} from '../Component/Setting/reposettings.model'
import {Project} from '../Component/Project/project.model'
import {GitlabBuild} from "../Component/Builds/gitlab.build.model";

@Injectable()

export class GitlabRequest extends AbstractCiRequest {
    private uri: string;

    constructor(http: Http, settings: Settings) {
        super(http, settings);
        this.uri = 'https://gitlab.com/api/v4/';
       // this.uri = "http://localhost:9002/gitlabmock.php/"
    }

    /**
     * @param {RepoSettings} settings
     * @returns {Promise<any>}
     */
    loadProjectData(settings: RepoSettings) {
        console.info('GIT LAB REQUEST -- getting projects for ' + settings.getName());
        let uri;
        uri = this.uri + "projects/" + encodeURIComponent(settings.getName());

        let headers = new Headers({
            'PRIVATE-TOKEN': settings.getToken(),
        });

        let options = new RequestOptions({headers: headers});

        let promise = new Promise((resolve, reject) => {
            this.http.get(uri, options).toPromise().then(
                res => {
                    let project = res.json();
                    let projectModel = new Project(
                        project.id,
                        settings.getName(),
                        false,
                        project.description,
                        project.last_activity_at,
                        project.last_activity_at,
                        0,
                        0,
                        'success'
                    );
                    resolve([projectModel]);
                },
                msg => { // Error
                    reject(msg);
                }
            )
        });
        return promise;
    }

    /**
     * @param {RepoSettings} settings
     * @param {string} slug
     * @param {number} id
     * @param buildId
     * @returns {Promise<any>}
     */
    getBuild(settings: RepoSettings, slug: string, id: number, buildId) {
        return new Promise((resolve, reject) => {
            resolve([]);
        });
    }

    /**
     *
     * @param {RepoSettings} settings
     * @param {string} slug
     * @param {number} id
     * @param {boolean} all
     * @returns {Promise<any>}
     */
    getBuilds(settings: RepoSettings, slug: string, id: number, all: boolean) {
        console.info('GIT LAB REQUEST -- getting builds for ' + settings.getName());
        let uri;
        uri = this.uri + "projects/" + encodeURIComponent(slug) + "/jobs";

        let headers = new Headers({
            'PRIVATE-TOKEN': settings.getToken(),
        });

        let options = new RequestOptions({headers: headers});

        let promise = new Promise((resolve, reject) => {
            this.http.get(uri, options).toPromise().then(res => {
                    let jobs = res.json();
                    let builds = [];
                    for (let job of jobs) {
                        let build = this.createBuildModel(job, slug);
                        builds.push(build);
                    }
                    resolve(builds);
                }, msg => { // Error
                    reject(msg);
                }
            )
        });

        return promise;
    }

    /**
     * Create a build mode from the retreived data.
     *
     * @param {Object} data
     * @param {Object} commit
     *
     * @returns {Build} Build object.
     */
    createBuildModel(data: any, slug) {
        let commitModel = new Commit(
            data.sha,
            data.ref,
            data.commit.author_email,
            data.commit.author_name,
            data.commit.message,
            '',
            data.user.avatar_url
        );
        return new GitlabBuild(
            data.id,
            data.id,
            slug,
            data.pipeline.status,
            data.created_at,
            data.finished_at,
            false,
            data.ref,
            data.ref,
            commitModel
        );
    }
}
