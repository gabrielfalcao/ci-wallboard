import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserSettings} from './usersettings.model';
import {Store} from "../../../Service/store";
import {Settings} from "../../../Service/settings";
import {CiRequestFactory} from "../../../Service/ciRequestFactory";
import {RepoSettings} from "../reposettings.model";
import {Toast, ToasterService} from "angular2-toaster";

@Component({
  templateUrl: './usersettings.component.html'
})

export class UserSettingsComponent implements OnDestroy, OnInit {
  public users: Array<UserSettings> = [];
  private store: Store = null;
  private requestFactory: CiRequestFactory;
  private settings: Settings;
  public showSaved: boolean = false;

  constructor(store: Store, requestFactory: CiRequestFactory, settings: Settings, private toasterService: ToasterService) {
    this.store = store;
    this.requestFactory = requestFactory;
    this.settings = settings;
    this.showSaved = false;
    this.loadUsers();

    if (this.users == null || this.users.length == 0) {
      this.newUser();
    }
  }

  newUser(): void {
    this.users = this.users || [];
    this.users.push(new UserSettings('New', ''));
  }

  getUsers(): Array<UserSettings> {
    return this.users;
  }

  saveUsers(): void {
    this.showSaved = true;
    const toast: Toast = {
      type: 'success',
      title: 'Saved',
      body: 'Your User was saved',
      showCloseButton: true,
    };

    this.toasterService.pop(toast);

    this.store.setValue('user-settings', JSON.stringify(this.users));
  }

  /**
   * Load users from the store and set them to the users variable.
   */
  loadUsers(): void {
    this.users = this.store.getValue('user-settings');
  }

  getHeading(i): string {
    return 'heading' + i;
  }

  getCollapse(i): string {
    return 'collapse' + i;
  }

  getTitle(user: RepoSettings, i: Number): string {
    return 'User:' + user.getName();
  }

  remove(index) {
    this.users.splice(index, 1);
  }

  ngOnDestroy(): void {
  }

  ngOnInit(): void {
    console.info('USER COMPONENT: init');
  }
}



