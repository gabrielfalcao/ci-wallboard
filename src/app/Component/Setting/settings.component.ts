import {Component} from '@angular/core';
import {RepoSettings} from './reposettings.model';
import {Store} from "../../Service/store";
import {CiRequestFactory} from "../../Service/ciRequestFactory";
import {TravisRequest} from "../../Service/travisRequest";
import {Toast, ToasterService} from 'angular2-toaster';

@Component({
    templateUrl: './settings.component.html',
    selector: 'settings'
})

export class SettingsComponent {
    users = [];
    githubToken = "";
    travisToken = "";

    request = null;
    environments = [
        {value: "travis", label: 'Travis'},
        {value: "bamboo", label: 'Bamboo'},
        {value: "gitlab", label: 'Gitlab'},
    ];
    refreshrates = [
        {value: 15, label: '15 sec'},
        {value: 30, label: '30 sec'},
        {value: 60, label: '1 min'},
        {value: 90, label: '1.5 min'},
        {value: 120, label: '2 min'},
        {value: 150, label: '2.5 min'},
        {value: 180, label: '3 min'},
        {value: 300, label: '5 min'},
        {value: 600, label: '10 min'},
        {value: 1800, label: '30 min'},
        {value: 3600, label: '1 hour'}
    ];

    constructor(private store: Store, private requestFactory: CiRequestFactory, private toasterService: ToasterService) {
        this.loadUsers();

        if (this.users == null || this.users.length == 0) {
            this.newUser();
        }
    }

    newUser() {
        this.users = this.users || [];
        this.users.push(new RepoSettings('', '', 30, 'travis', ''));
    }

    getUsers() {
        return this.users;
    }

    showUsers() {
        // console.info(this.users);
    }

    saveUsers() {
        // this.toasterService.pop('success', 'Saved', 'Your Repository was saved');
        var toast : Toast = {
            type: 'success',
            title: 'Saved',
            body: 'Your Repository was saved',
            showCloseButton: true,
        };

        this.toasterService.pop(toast);

        this.store.setValue('users', JSON.stringify(this.users));
    }

    /**
     * Load users from the store and set them to the users variable.
     */
    loadUsers() {
        this.users = this.store.getValue('users');
    }

    /**
     * Do a requestFactory to travis with an github token to generate a travis token.
     */
    generateToken() {
        let travisCi: TravisRequest;
        travisCi = this.requestFactory.create('travis') as TravisRequest;
        travisCi.getTravisToken(this.githubToken).subscribe(token => this.travisToken = token.access_token);
    }

    getHeading(i) {
        return 'heading' + i;
    }

    getCollapse(i) {
        return 'collapse' + i;
    }

    getTitle(user: any, i: Number): string {
        let access = "";
        if (user.token !== "") {
            access = " Private ";
        } else {
            access = " Public ";
        }

        if (user.name === "") {
            return "New repo " + i;
        }

        return "[[" + user.environment + "]] " + user.name + " Repository (" + access + ')';

    }

    remove(index) {
        this.users.splice(index, 1);
    }
}



