'use strict';

import {Component, Output, Input} from '@angular/core';

@Component({
    selector: 'success-item',
    templateUrl: './status.success.html'
})

export class StatusSuccessComponent {
    @Input()
    build;
    pos;

    getBuild() {
        return this.build.val;
    }

    getClasses() {
        let index = this.build.index;
        if (index <= 3) {
            return 'col-md-4 first-row';
        } else if (index > 3 && index <= 7) {
            return 'col-md-3 second-row';
        } else if (index > 7 && index <= 13) {
            return 'col-md-2 third-row';
        } else {
            return 'col-md-1 other-rows';
        }
    }
}
