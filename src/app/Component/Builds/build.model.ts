'use strict';
import {Commit} from './commit.model';

export class Build {
    private _id: number;
    private _number: number;
    private _name: string;
    private _state: string;
    private _startedAt: string;
    private _finishedAt: string;
    private _isPr: boolean;
    private _prnr: number;
    private _type: string;
    private _commit: Commit;

    constructor(id: number, number: number, name: string, state: string, startedAt: string, finishedAt: string, isPr: boolean, prnr: number, type: string, commit: Commit) {
        this._id = id;
        this._number = number;
        this._name = name;
        this._state = state;
        this._startedAt = startedAt;
        this._finishedAt = finishedAt;
        this._isPr = isPr;
        this._prnr = prnr;
        this._type = type;
        this._commit = commit;
    }

    getId(): number {
        return this._id;
    }

    getType(): string {
        return this._type;
    }

    getNumber(): number {
        return this._number;
    }

    getName(): string {
        return this._name;
    }

    getState(): string {
        return this._state;
    }

    getStartedAt(): string {
        return this._startedAt;
    }

    getFinishedAt(): string {
        return this._finishedAt;
    }

    isPr(): boolean {
        return this._isPr;
    }

    getPrnr(): number {
        return this._prnr;
    }

    /**
     *
     * @returns {Commit}
     */
    getCommit(): Commit {
        return this._commit;
    }

    isBuilding(): boolean {
        return this._state === 'started' || this._state === 'created' || this._state === 'received';
    }

    isPassing(): boolean {
        return this._state === 'passed' || this._state === 'canceled' || this._state === 'finished';
    }

    isFailed(): boolean {
        return this._state === 'failed' || this._state === 'error' || this._state === 'errored';
    }

    hasRecentError(): boolean {
        let dt = new Date(Date.parse(this._finishedAt));
        let now = new Date();

        let diff = now.getTime() - dt.getTime();

        let minutes = Math.floor((diff / (60000)));

        if (minutes < 5) {
            return true;
        }
        return false;
    }
}
